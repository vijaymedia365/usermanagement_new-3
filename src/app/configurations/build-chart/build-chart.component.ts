import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { ChartCongigService } from '../../shared/data/chart-congig.service';


@Component({
  selector: 'app-build-chart',
  templateUrl: './build-chart.component.html',
  styleUrls: ['./build-chart.component.scss']
})
export class BuildChartComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  regularForm: FormGroup;
  columns;
  isCollapsed = true;


  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  tblcolumns = [
      { prop: 'ChartName' },
      { prop: 'ChartType' },
      // { name: 'Preview' },
      { name: 'Action' }
  ];

  valueName;
  newsletter = false;
  newsletterSelectedColumn = '';
  userData = true;
  between = false;
  memLoyVal = false;
  newsletterColumns = '';
  chartsCount;
  charts;

  constructor(public chartService: ChartCongigService) {}

  ngOnInit() {
    this.regularForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'chartType': new FormControl(null, [Validators.required]),
      'data': new FormControl(null, [Validators.required]),
      'xaxsis': new FormControl(null, [Validators.required]),
      'yaxsis': new FormControl(null, [Validators.required]),
      'columns': new FormControl(null, [Validators.required]),
      'daterange': new FormControl(null, [Validators.required]),
      'newscolumns': new FormControl(null, [Validators.required]),
      'xdata': new FormControl(null, [Validators.required])
    }, {updateOn: 'blur'});
    // this.chartService.getUserColumn().then(data => {
    //   if(data['status'] == 'success'){
    //     this.columns = data['columns'];
    //   }
    // });
    this.chartService.getCharts(1).then(data => {
      if(data['status'] == 'success'){
        this.charts = data['charts'];
        this.chartsCount = data['charts'].length;
        setTimeout(() => { this.loadingIndicator = false; }, 1500);
      }
    });
  }

  selectColumn(tableVal, event){
    // if(val == 'user'){
    //   this.memLoyVal = false;
    //   this.newsletter = false;
    //   this.newsletterSelectedColumn = '';
    //   this.regularForm.controls['newscolumns'].setValue('');
    // }
    // else{
    //   this.memLoyVal = true;
    // }
    this.valueName = event.target.options[event.target.options.selectedIndex].text;
    if(tableVal == 'users'){
      this.chartService.getColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
      this.userData = true;
      this.memLoyVal = false;
      this.newsletter = false;
      this.newsletterSelectedColumn = '';
      this.regularForm.controls['newscolumns'].setValue('');
    }
    else{
      this.userData = false;
      if(tableVal == 'newsletter'){
        this.newsletter = true;
      }
      else{
        this.newsletter = false;
        this.newsletterSelectedColumn = '';
        this.regularForm.controls['newscolumns'].setValue('');
      }
      this.memLoyVal = true;
      this.chartService.getParticularColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
    }
  }

  getstatscolumns(event){
    this.chartService.getnewsletterColumns().then(data => {
      if(data['status'] == 'success'){
        this.newsletterColumns = data['columns'];
        //console.log(this.columns);
      }
    });
  }

  onReactiveFormSubmit(){
    let chartDetail = {
      uid: 1, //login userId
      name: this.regularForm.controls['name'].value,
      chartType: this.regularForm.controls['chartType'].value,
      data: this.regularForm.controls['data'].value,
      xaxsis: this.regularForm.controls['xaxsis'].value,
      yaxsis: this.regularForm.controls['yaxsis'].value,
      columns: (this.userData) ? this.regularForm.controls['columns'].value : '',
      daterange: (this.userData) ? this.regularForm.controls['daterange'].value : '',
      newsletterColumns: this.newsletterSelectedColumn,
      xdata: (this.userData) ? this.regularForm.controls['xdata'].value : '',
      subTableName: this.regularForm.controls['newscolumns'].value,
      mainRecordId: (!this.userData) ? this.regularForm.controls['columns'].value : ''
    }
    console.log(chartDetail);
    this.chartService.generateUserChart(chartDetail).then(data => {
      if(data['status'] == 'success'){
        alert('done');
      }
    });
  }

  cancelEdit(){
    this.regularForm.reset();
  }

  deleteChart(id){
    this.chartService.deleteChart(id).then(data => {
      if(data['status'] == 'success'){
        window.location.reload();
      }
    });
  }

}
