import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { iconData, colorPalette, simpleIconData } from '../../shared/data/icon-data.service';

import { ChartCongigService } from '../../shared/data/chart-congig.service';

@Component({
  selector: 'app-tiles-config',
  templateUrl: './tiles-config.component.html',
  styleUrls: ['./tiles-config.component.scss']
})
export class TilesConfigComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  icons;
  selectedIcon = '';
  columns;
  newsletterColumns;
  colorPalettes;
  regularForm: FormGroup;
  memLoyVal = false;
  valueName;
  simpleIcon;
  tilesCount;
  newsletter = false;
  newsletterSelectedColumn = '';
  tiles;
  isCollapsed = true;

  constructor(public chartService: ChartCongigService) {
    this.icons = iconData;
    this.colorPalettes = colorPalette;
    this.simpleIcon = simpleIconData;
    //GET TILES Count
    this.chartService.getTilesCount(1).then(data => {
      if(data['status'] == 'success'){
        this.tilesCount = data['count'];
      }
    });
  }

  ngOnInit() {
    this.regularForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'value': new FormControl(null, [Validators.required]),
      'columns': new FormControl(null, [Validators.required]),
      'color': new FormControl(null, [Validators.required]),
      'newscolumns': new FormControl(null, [Validators.required])
    }, {updateOn: 'blur'});
    this.chartService.getTiles(1).then(data => {
      if(data['status'] == 'success'){
        this.tiles = data['tiles'];
      }
    });
  }

  selecIcon(iconVal){
    this.selectedIcon = iconVal;
  }

  selectColumn(tableVal, event){
    if(tableVal == 'users'){
      this.chartService.getColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
      this.memLoyVal = false;
      this.newsletter = false;
      this.newsletterSelectedColumn = '';
      this.regularForm.controls['newscolumns'].setValue('');
    }
    else{
      if(tableVal == 'newsletter'){
        this.newsletter = true;
      }
      else{
        this.newsletter = false;
        this.newsletterSelectedColumn = '';
        this.regularForm.controls['newscolumns'].setValue('');
      }
      this.memLoyVal = true;
      this.valueName = event.target.options[event.target.options.selectedIndex].text;
      this.chartService.getParticularColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
    }
  }

  getstatscolumns(event){
    this.chartService.getnewsletterColumns().then(data => {
      if(data['status'] == 'success'){
        this.newsletterColumns = data['columns'];
        //console.log(this.columns);
      }
    });
  }

  onReactiveFormSubmit(){
    let tilesData = {
      name: this.regularForm.controls['name'].value,
      value: this.regularForm.controls['value'].value,
      columns: (this.memLoyVal) ? '' : this.regularForm.controls['columns'].value,
      color: this.regularForm.controls['color'].value,
      icon: this.selectedIcon,
      uId: 1,
      subTableName: this.regularForm.controls['newscolumns'].value,
      mainRecordId: (this.memLoyVal) ? this.regularForm.controls['columns'].value : '',
      newsletterColumns: this.newsletterSelectedColumn
    }
    console.log(tilesData);
    this.chartService.generateTiles(tilesData).then(data => {
      if(data['status'] == 'success'){
        window.location.reload();
      }
    });
  }

  cancel(){
    this.regularForm.reset();
  }

  deleteTile(id){
    this.chartService.deleteTile(id).then(data => {
      if(data['status'] == 'success'){
        window.location.reload();
      }
    });
  }
}
