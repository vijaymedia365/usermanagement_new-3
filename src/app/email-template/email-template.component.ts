import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailTemplateService } from '../shared/data/email-template.service';

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss']
})
export class EmailTemplateComponent implements OnInit {
	emailTemplateForm: FormGroup;
	defaultTemplate;
	emailTemplates;
	p: number = 1;
	pageSize: number = 5;
	collectionSize;
	public showTable = true;
	public showEdit = false;
	public new = true;
	public emailTemDetails: any;
	public options: Object = {
	    charCounterCount: true,
	    // Set the image upload parameter.
	    imageUploadParam: 'image_param',

	    // Set the image upload URL.
	    imageUploadURL: 'https://click365.com.au/usermanagement/images',

	    // Additional upload params.
	    imageUploadParams: {id: 'my_editor'},

	    // Set request type.
	    imageUploadMethod: 'POST',

	    // Set max image size to 5MB.
	    imageMaxSize: 5 * 1024 * 1024,

	    // Allow to upload PNG and JPG.
	    imageAllowedTypes: ['jpeg', 'jpg', 'png'],
    	events:  {
			'froalaEditor.initialized':  function () {
				console.log('initialized');
			},
  			'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
			    //Your code
			    if  (images.length) {
					// Create a File Reader.
					const  reader  =  new  FileReader();
					// Set the reader to insert images when they are loaded.
					reader.onload  =  (ev)  =>  {
					const  result  =  ev.target['result'];
					editor.image.insert(result,  null,  null,  editor.image.get());
					console.log(ev,  editor.image,  ev.target['result'])
					};
					// Read image as base64.
					reader.readAsDataURL(images[0]);
		    	}
		    	// Stop default upload chain.
		    	return  false;
		  	}
		}
	};

	editEmailTemDetails = {
		'id': '',
		'name': '',
		'subject': '',
		'body': ''
	};

	constructor(private formBuilder: FormBuilder, public emailtemplateservice: EmailTemplateService, public router: Router) {
		this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();
	}

	ngOnInit() {
		  this.emailTemplateForm = this.formBuilder.group({
              temname: ['', Validators.required],
  			temsub: ['', Validators.required],
  			tembody: this.defaultTemplate,
        template:false
        });
        this.emailtemplateservice.getEmailTemp().then(data => {
	        console.log(data['emailTemplate']);
	        if(data['status'] == 'success'){
	            //for (let i = 0; i < data['emailTemplate'].length; i++) {emailtemplate.push(data['emailTemplate'][i]); }
	        	this.emailTemplates = data['emailTemplate'];
	        	this.collectionSize = data['emailTemplate'].length;
	          }
	    },
	    error => {
	    });
	}

	get f() { return this.emailTemplateForm.controls; }

	addet(){
		this.showTable = false;
		this.new = true;
	}

	cancel(){
		window.location.reload();
	}

	editet(id){
        this.emailtemplateservice.getEmailTempById(id).then(data => {
	        console.log(data['emailTemplate']);
	        this.editEmailTemDetails.id = data['emailTemplate'][0].id;
	        this.editEmailTemDetails.name = data['emailTemplate'][0].name;
	        this.editEmailTemDetails.subject = data['emailTemplate'][0].subject;
	        this.editEmailTemDetails.body = data['emailTemplate'][0].body;

			this.new = false;
			this.showEdit = true;
	    },
	    error => {
	    });
	}

	deleteet(id){
        this.emailtemplateservice.deleteEmailTempById(id).then(data => {
	        console.log(data['emailTemplate']);
	        window.location.reload();
	    },
	    error => {
	    });
	}

	addEmailTemplate(){
		this.emailTemDetails = {
			name: this.f.temname.value,
			subject: this.f.temsub.value,
			body: this.f.tembody.value,
			type: this.f.template.value.toString()
		}
		this.emailtemplateservice.addEmailTemp(this.emailTemDetails).then(data => {
			if(data['status']){
				//window.location.reload();
				this.router.navigate(['/email-template']);
			}
		});
	}

	updateEmailTemplate(){
		this.emailtemplateservice.updateEmailTemp(this.editEmailTemDetails).then(data => {
			if(data['status']){
				this.showTable = true;
				this.showEdit = false;
				this.new = true;
				window.location.reload();
			}
		});
	}

}
