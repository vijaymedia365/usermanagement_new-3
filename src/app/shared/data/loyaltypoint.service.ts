import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class loyaltypointService {
    constructor(private http: HttpClient) { }
    public timestamp = new Date().getTime();

    getloyaltypoints(listId) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/loyaltypoint.php?q='+this.timestamp+'&action=get&listId='+listId)
            .subscribe(name => {
                console.log(name);
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    getallloyaltypoints(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/loyaltypoint.php?q='+this.timestamp+'&action=getall')
            .subscribe(name => {
                console.log(name);
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    addloyaltypoint(loyaltypoint) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/loyaltypoint.php?q='+this.timestamp+'&action=add', { loyaltypoint })
            .subscribe(name => {
                console.log(name);
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
}
