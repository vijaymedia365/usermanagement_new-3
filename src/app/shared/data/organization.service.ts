import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class OrganizationService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    saveOrganization(organizationDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveOrganization.php?q='+this.timestamp, { organizationDetail })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);               
                resolve(subscriptionValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    
    getOrganization(timestamp) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getOrganization.php?action=getorganization&q='+timestamp)
            .subscribe(data => {
                console.log(data);               
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getOrganizationById(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?action=getorganizationbyid&q='+this.timestamp, { id })
            .subscribe(data => {
                console.log(data);               
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getCharts() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=getChartsForAdmin&q='+this.timestamp)
            .subscribe(data => {
                console.log(data);               
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getTiles() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=getTilesForAdmin&q='+this.timestamp)
            .subscribe(data => {
                console.log(data);               
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getListCount() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=getReportsForAdmin&q='+this.timestamp)
            .subscribe(data => {
                console.log(data);               
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    updateStatus(status, id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=update&q='+this.timestamp, {status,id})
            .subscribe(data => {
                console.log(data);               
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}