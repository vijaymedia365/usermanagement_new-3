import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class SubscriptionService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    saveSubscription(subscriptionDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+this.timestamp, { subscriptionDetail })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);               
                resolve(subscriptionValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    
    getSubscription() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getSubscription.php?q='+this.timestamp)
            .subscribe(settingValue => {
                console.log(settingValue);               
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getSubscriptionByType(type) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getSubscriptionByType.php?q='+this.timestamp+'&action=get',{ type })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }

    getOrgSubscriptionById(userid) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?q='+this.timestamp+'&action=getorgsubscriptionbyid',{ userid })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }

    updateSubscriptionType(type,id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=updatesubtype&q='+this.timestamp, {type,id})
            .subscribe(data => {
                console.log(data);               
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}