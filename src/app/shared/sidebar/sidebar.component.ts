import { Component, OnInit } from '@angular/core';
import { ROUTES, SUPERADMINROUTES, USERROUTES, LISTADMINROUTES } from './sidebar-routes.config';
import { RouteInfo } from "./sidebar.metadata";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';

declare var $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public superAdminmenuItems: any[];
    public userMenuItems: any[];
    public listAdminMenuItems: any[];
    public isListAdmin = false;
    public isAdmin = true;
    public isSuperAdmin = false;

    constructor(private router: Router, private route: ActivatedRoute, public translate: TranslateService) {
        let users = JSON.parse(localStorage.getItem('currentUser'));
        if(users){
            if(users['role_id'] == '2'){
                this.isSuperAdmin = true;
                this.isAdmin = false;
                this.isListAdmin = false;
            }
            else if(users['role_id'] == '4'){  // list admin role id
                this.isListAdmin = true;
                localStorage.setItem('isListAdmin', 'true');
            }
            else if (users['role_id'] != "1" && users['role_id'] != "2"){ //admin / super admin role id
              this.isAdmin = false;          
            }
        }
    }

    ngOnInit() {
        $.getScript('./assets/js/app-sidebar.js');
        
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        this.superAdminmenuItems = SUPERADMINROUTES.filter(menuItem => menuItem);
        this.userMenuItems = USERROUTES.filter(menuItem => menuItem);
        this.listAdminMenuItems = LISTADMINROUTES.filter(menuItem => menuItem);
    }

}
