import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

import { UserService } from '../../shared/data/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  regularForm: FormGroup;
  userId;
  gender;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  };
  newaddress;
  submitted = false;

  constructor(public userservice: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
      this.regularForm = new FormGroup({
        'username': new FormControl(null, [Validators.required]),
        'firstName': new FormControl(null, [Validators.required]),
        'email': new FormControl(null, [Validators.required, Validators.email]),
        'dob': new FormControl(null, [Validators.required]),
        'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        'lastName': new FormControl(null, [Validators.required]),
        'genderRadios': new FormControl('Female'),
        'address': new FormControl(null, [Validators.required]),
          // 'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
          // 'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
          // 'textArea': new FormControl(null, [Validators.required]),
          // 'radioOption': new FormControl('Option one is this')
      }, {updateOn: 'blur'});
      this.route.queryParams.subscribe(params => {this.userId = params['id'];})
    	this.userservice.getUser(this.userId).then(data => {
			if(data['status'] == 'success'){
				this.regularForm.controls['username'].setValue(data['user'][0].username);
				this.regularForm.controls['firstName'].setValue(data['user'][0].firstName);
				this.regularForm.controls['email'].setValue(data['user'][0].email);
				this.regularForm.controls['dob'].setValue(data['user'][0].dob);
				this.regularForm.controls['password'].setValue(data['user'][0].password);
				this.regularForm.controls['lastName'].setValue(data['user'][0].lastName);
				this.regularForm.controls['address'].setValue(data['user'][0].address);
				this.regularForm.controls['genderRadios'].setValue(data['user'][0].gender);
        this.gender = data['user'][0].gender;
			}
		});
  }

  onReactiveFormSubmit() {
    this.submitted = true;
      let userDetail = {
  			username: this.regularForm.controls['username'].value,
  			password: this.regularForm.controls['password'].value,
  			firstName: this.regularForm.controls['firstName'].value,
  			lastName: this.regularForm.controls['lastName'].value,
  			email: this.regularForm.controls['email'].value,
  			dob: this.regularForm.controls['dob'].value,
  			genderRadios: this.regularForm.controls['genderRadios'].value,
  			address: this.newaddress,
        phone: '',
        profileImage: '',
        id: this.userId,
        organization: 1
  		}
      this.userservice.editUser(userDetail).then(data => {
			if(data['status'] == 'success'){
        this.regularForm.reset();
				this.router.navigate(['/users/users']);
			}
      else{
        alert('error');
      }
		});
  }

  cancelEdit(){
    this.regularForm.reset();
    this.router.navigate(['/users/users']);
  }

  handleAddressChange(address) {
    // Do some stuff
    this.newaddress = address.formatted_address;
  }

}
